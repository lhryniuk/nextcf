request = require 'request'
readline = require 'readline'

cfApiUrl = 'http://codeforces.com/api/'

rl = readline.createInterface { input: process.stdin, output: process.stdout }

cfTags = [
  '2-sat',
  'binary search',
  'bitmasks',
  'brute force',
  'chinese remainder theorem',
  'combinatorics',
  'constructive algorithms',
  'data structures',
  'dfs and similar',
  'divide and conquer',
  'dp',
  'dsu',
  'expression parsing',
  'fft',
  'flows',
  'games',
  'geometry',
  'graphs',
  'graph matchings',
  'greedy',
  'hashing',
  'implementation',
  'math',
  'matrices',
  'meet-in-the-middle',
  'number theory',
  'probabilities',
  'schedules'
  'shortest paths',
  'sortings',
  'strings',
  'string suffix structures',
  'ternary search',
  'trees',
  'two pointers',
]

getSortedTags = () ->
  new Promise (resolve, reject) ->
    url = "#{cfApiUrl}problemset.problems"
    request.get {uri: url, json: true}, (err, response, body) ->
      problems = body['result']['problems']
      tags = new Set
      tags.add t for t in p['tags'] for p in problems
      resolve(Array.from(tags).sort())

getProblemsWithTag = (tags) ->
  new Promise (resolve, reject) ->
    tagsStr = tags.join ';'
    url = "#{cfApiUrl}problemset.problems?tags=#{tagsStr}"
    request.get {uri: url, json: true}, (err, response, body) ->
      if not err? and response.statusCode == 200
        resp = body['result']['problems']
        resolve resp

showProblem = (problem) ->
  problemUrl = "http://codeforces.com/problemset/problem/"
  "[#{problem["index"]}] #{problem['name']} #{problemUrl}#{problem['contestId']}/#{problem['index']}"

console.log "Available tags:\n" + ("#{1 + cfTags.indexOf(tag)} #{tag}" for tag in cfTags).join('\n')
rl.question 'Select desired tags numbers, separated by commas: ', (tagNumbers) ->
  tags = (cfTags[parseInt(i) - 1] for i in tagNumbers.split(','))

  rl.question 'Select level (A-H) ', (level) ->
    (getProblemsWithTag tags)
      .then (v) ->
        if v?
          problemsOnLevel = (p for p in v when p['index'] == level)
          console.log((showProblem p for p in problemsOnLevel).join("\n"))
        else
          console.log("Cannot find problems with given tags.")

    rl.close()
