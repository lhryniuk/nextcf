# nextcf

`nextcf` should help you choose your next problem on **Codeforces** site.

Basing on your level and chose category (and some statistics about contests
participants in the future) it will propose next problem, intentionally a bit
above your level, to let you train your skills. **For know, it only allows to
choose select tags of it**.

Before running, execute in a project dir:

```shell
npm install
```

and next:

```shell
coffee nextcf.coffee
```

After selecting tags, you'll get list of problems with difficulty level, name
and URL.
